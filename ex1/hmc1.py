import ctypes
import ppmd as md
import numpy as np
np.set_printoptions(threshold=10000)

mpi_rank = md.mpi.MPI.COMM_WORLD.Get_rank()
const = md.kernel.Constant
READ = md.access.READ
INC_ZERO = md.access.INC_ZERO
INC = md.access.INC
ParticleLoop = md.loop.ParticleLoopOMP
PairLoop = md.pairloop.PairLoopNeighbourListNSOMP

nx = 10
ny = 10
nz = 10
N = nx * ny * nz
E = (50.0, 50.0, 50.0)
r_c = 10.0
r_n = 1.1 * r_c
dt = 0.01
mass = 1.0
mass_mom = 1.0

# lj vars
epsilon = 1.0
sigma = 1.0
C_V = 4. * epsilon
C_F = -48 * epsilon / sigma ** 2
r_n2 = r_n * r_n
r_c2 = r_c * r_c
sigma2 = sigma * sigma
lj_shift = (sigma / r_c) ** 6 - (sigma / r_c) ** 12

# barrier vars
b_z0 = (1. / 3.) * E[2] - 0.5 * E[2]
b_z1 = (2. / 3.) * E[2] - 0.5 * E[2]
b_width = 1.0
b_height = 1.0

A = md.state.State()
A.npart = N
A.domain = md.domain.BaseDomainHalo(extent=E)
A.domain.boundary_condition = md.domain.BoundaryTypePeriodic()

A.pos = md.data.PositionDat(ncomp=3)
A.vel = md.data.ParticleDat(ncomp=3)
A.force = md.data.ParticleDat(ncomp=3)

A.pos_prev = md.data.ParticleDat(ncomp=3)
A.vel_prev = md.data.ParticleDat(ncomp=3)

A.pos_mom = md.data.ParticleDat(ncomp=3)
A.vel_mom = md.data.ParticleDat(ncomp=3)

A.id = md.data.ParticleDat(ncomp=1, dtype=ctypes.c_long)

# globals
A.pe = md.data.GlobalArray(ncomp=1, dtype=ctypes.c_double)
A.ke = md.data.GlobalArray(ncomp=1, dtype=ctypes.c_double)
A.ke_mom = md.data.GlobalArray(ncomp=1, dtype=ctypes.c_double)

rng = np.random.RandomState(seed=(mpi_rank + 1) * 0x585FAB)

if mpi_rank == 0:

    A.pos[:] = md.utility.lattice.cubic_lattice(n=(nx, ny, nz), e=E)
    A.vel[:] = 0.0

    A.pos_prev[:] = 0.0
    A.vel_prev[:] = 0.0

    A.pos_mom[:] = rng.normal(size=(N, 3))
    A.vel_mom[:] = rng.normal(size=(N, 3))

    A.id[:, 0] = range(N)

A.scatter_data_from(0)


constants = (
    const('dt', dt),
    const('hdt', 0.5 * dt),
    const('mass', mass),
    const('imass', 1.0 / mass),
    const('mass_mom', mass_mom),
    const('imass_mom', 1.0 / mass_mom),
    const('sigma2', sigma2),
    const('rc2', r_c2),
    const('internalshift', lj_shift),
    const('CF', C_F),
    const('CV', C_V),
    const('z0', b_z0),
    const('z1', b_z1),
    const('b_height', b_height),
    const('ib_width2', 1. / (b_width * b_width)),
)

lf1 = ParticleLoop(
    kernel=md.kernel.Kernel(
        'hmc_lf1',
        """
        // half update of momentum vars
        pos_mom.i[0] -= hdt*force.i[0];
        pos_mom.i[1] -= hdt*force.i[1];
        pos_mom.i[2] -= hdt*force.i[2];
        vel_mom.i[0] -= hdt*imass*vel.i[0];
        vel_mom.i[1] -= hdt*imass*vel.i[1];
        vel_mom.i[2] -= hdt*imass*vel.i[2];
        // full update of "real" vars
        pos.i[0] += dt*imass_mom*pos_mom.i[0];
        pos.i[1] += dt*imass_mom*pos_mom.i[1];
        pos.i[2] += dt*imass_mom*pos_mom.i[2];
        vel.i[0] += dt*imass_mom*vel_mom.i[0];
        vel.i[1] += dt*imass_mom*vel_mom.i[1];
        vel.i[2] += dt*imass_mom*vel_mom.i[2];
        // update "real" kinetic energy
        ke[0] += 0.5*mass*(
            vel.i[0]*vel.i[0] +
            vel.i[1]*vel.i[1] +
            vel.i[2]*vel.i[2]
        );
        """,
        constants
    ),
    dat_dict={
        'pos_mom': A.pos_mom(INC),
        'vel_mom': A.vel_mom(INC),
        'pos': A.pos(INC),
        'vel': A.vel(INC),
        'force': A.force(READ),
        'ke': A.ke(INC_ZERO)
    }
)

lf2 = ParticleLoop(
    kernel=md.kernel.Kernel(
        'hmc_lf2',
        """
        // half update of momentum vars
        pos_mom.i[0] -= hdt*force.i[0];
        pos_mom.i[1] -= hdt*force.i[1];
        pos_mom.i[2] -= hdt*force.i[2];
        vel_mom.i[0] -= hdt*imass*vel.i[0];
        vel_mom.i[1] -= hdt*imass*vel.i[1];
        vel_mom.i[2] -= hdt*imass*vel.i[2];
        ke_mom[0] += 0.5*mass_mom*(
            vel.i[0]*vel.i[0] +
            vel.i[1]*vel.i[1] +
            vel.i[2]*vel.i[2]
        );
        """,
        constants
    ),
    dat_dict={
        'pos_mom': A.pos_mom(INC),
        'vel_mom': A.vel_mom(INC),
        'vel': A.vel(INC),
        'force': A.force(READ),
        'ke_mom': A.ke(INC_ZERO)
    }
)

lj_pairloop = PairLoop(
    kernel=md.kernel.Kernel(
        'hmc_lj',
        """
        const double R0 = pos.j[0] - pos.i[0];
        const double R1 = pos.j[1] - pos.i[1];
        const double R2 = pos.j[2] - pos.i[2];

        const double r2 = R0*R0 + R1*R1 + R2*R2;

        const double r_m2 = sigma2/r2;
        const double r_m4 = r_m2*r_m2;
        const double r_m6 = r_m4*r_m2;

        pe[0]+= (r2 < rc2) ? 0.5*CV*((r_m6-1.0)*r_m6 + internalshift) : 0.0;

        const double r_m8 = r_m4*r_m4;
        const double f_tmp = CF*(r_m6 - 0.5)*r_m8;

        force.i[0] += (r2 < rc2) ? f_tmp*R0 : 0.0;
        force.i[1] += (r2 < rc2) ? f_tmp*R1 : 0.0;
        force.i[2] += (r2 < rc2) ? f_tmp*R2 : 0.0;
        """,
        constants
    ),
    dat_dict={
        'pos': A.pos(READ),
        'force': A.force(INC),
        'pe': A.pe(INC)
    },
    shell_cutoff=r_n
)

eng_bar = ParticleLoop(
    kernel=md.kernel.Kernel(
        'hmc_energy_barrier',
        """
        const double dz0 = pos.i[2] - z0;
        const double dz1 = pos.i[2] - z1;
        const double exp_z0 = exp(-0.5 * dz0 * dz0 * ib_width2);
        const double exp_z1 = exp(-0.5 * dz1 * dz1 * ib_width2);
        const double utmp = b_height * (exp_z0 + exp_z1);
        const double ftmp = b_height * ib_width2 * (dz0 * exp_z0 + dz1 * exp_z1);
        pe[0] += utmp;
        force.i[0] += 0.0;
        force.i[1] += 0.0;
        force.i[2] += ftmp;
        """,
        constants
    ),
    dat_dict={
        'pos': A.pos(READ),
        'force': A.force(INC_ZERO),
        'pe': A.pe(INC_ZERO)
    }
)

for it in md.method.IntegratorRange(10000, dt, A.vel, 10, r_n, verbose=True):
    eng_bar.execute()
    lj_pairloop.execute()
    lf1.execute()

    eng_bar.execute()
    lj_pairloop.execute()
    lf2.execute()









